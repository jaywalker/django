# Django

#### 介绍
    模拟简单水果下单系统
    #############Python Web天天生鲜系统###########

#### 项目运行环境
    PyCharm 2019.3.2
    Python 3.6.8
    Django 3.2.16
    MySQL 8.0
    Navicat Premium 11.0.8

#### 项目构成
    
    templates：html文件
    static：静态资源文件
    dailyfresh：项目整体配置 
    user：用户功能模块
    order：购物车功能模块
    goods：商品功能模块
    cart：购物车功能模块

#### 项目功能展示
![输入图片说明](imgimage.png)
![输入图片说明](image.png)
![输入图片说明](1image.png)
![输入图片说明](1.1image.png)
![输入图片说明](2image.png)
![输入图片说明](2.1image.png)
![输入图片说明](3image.png)
![输入图片说明](4image.png)
![输入图片说明](5image.png)
![输入图片说明](6image.png)
![输入图片说明](7image.png)
#### 后续优化
    增加支付宝、微信支付等付款跳转。
    在提交订单页面增加多个收货地址的选项按钮。
    下单成功回复下单成功短信通知等等的优化处理。
    继续后台优化代码逻辑提高运行效率。
    