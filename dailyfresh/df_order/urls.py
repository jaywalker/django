from django.urls import path
from django.conf.urls import url
from . import views

# 子路由
urlpatterns = [
    url('^$', views.index,name='index'),
    url('^get_checkedgoods$', views.get_checkedgoods, name='get_checkedgoods'),
    url('^add_checkedgoods$', views.add_checkedgoods, name='add_checkedgoods'),
    url('^delete(\d+)$', views.delete, name='delete'),

]