from django.db import models
from django.db import models
from df_user.models import *
from df_goods.models import *
from tinymce.models import HTMLField


# Create your models here.
class OrderInfo(models.Model):
    order_id = models.CharField(max_length=64, primary_key=True)
    user = models.ForeignKey(UserInfo, on_delete=models.CASCADE)
    o_phone = models.IntegerField()
    o_address = models.CharField(max_length=50)
    o_time = models.CharField(max_length=50)
    o_paymethod = HTMLField()
    o_status = HTMLField(default="未支付")
    o_species_count = models.IntegerField()
    o_freight = models.CharField(max_length=20, default=10)
    o_total = models.IntegerField()


class Order_GoodsInfo(models.Model):
    order = models.ForeignKey(OrderInfo, to_field='order_id', on_delete=models.CASCADE)
    goods = models.ForeignKey(GoodsInfo, on_delete=models.CASCADE)
    count = models.IntegerField()
    price = models.CharField(max_length=20)
    subtotal = models.CharField(max_length=20)
