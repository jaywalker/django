from django.apps import AppConfig


class DfOrderConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'df_order'
