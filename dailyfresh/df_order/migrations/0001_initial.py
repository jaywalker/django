# Generated by Django 3.2.16 on 2023-01-12 08:37

from django.db import migrations, models
import django.db.models.deletion
import tinymce.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('df_goods', '0001_initial'),
        ('df_user', '0002_auto_20230104_1605'),
    ]

    operations = [
        migrations.CreateModel(
            name='OrderInfo',
            fields=[
                ('order_id', models.CharField(max_length=64, primary_key=True, serialize=False)),
                ('o_phone', models.IntegerField()),
                ('o_address', models.CharField(max_length=50)),
                ('o_time', models.CharField(max_length=50)),
                ('o_paymethod', tinymce.models.HTMLField()),
                ('o_status', tinymce.models.HTMLField(default='未支付')),
                ('o_species_count', models.IntegerField()),
                ('o_freight', models.CharField(default=10, max_length=20)),
                ('o_total', models.IntegerField()),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='df_user.userinfo')),
            ],
        ),
        migrations.CreateModel(
            name='Order_GoodsInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.IntegerField()),
                ('price', models.CharField(max_length=20)),
                ('subtotal', models.CharField(max_length=20)),
                ('df_goods', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='df_goods.goodsinfo')),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='df_order.orderinfo')),
            ],
        ),
    ]
