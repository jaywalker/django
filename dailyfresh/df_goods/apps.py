from django.apps import AppConfig


class DfGoodsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'df_goods'
