from django.shortcuts import render
from df_user.models import *
from df_cart.models import *

from django.http import HttpResponse
from df_goods.models import GoodsInfo, TypeInfo
from django.core.paginator import Paginator
# Create your views here.

def index(request):
    uid = request.session.get('user_id')
    result = CartInfo.objects.filter(user_id=uid).count()
    fruit = GoodsInfo.objects.filter(gtype_id=1).order_by('-id')[:3]  # 最新商品
    fruit2 = GoodsInfo.objects.filter(gtype_id=1).order_by('-gclick')[:4]  # 热销商品
    seafood = GoodsInfo.objects.filter(gtype_id=2).order_by('-id')[:3]  # 最新商品
    seafood2 = GoodsInfo.objects.filter(gtype_id=2).order_by('-gclick')[:4]  # 热销商品
    meat = GoodsInfo.objects.filter(gtype_id=3).order_by('-id')[:3]  # 最新商品
    meat2 = GoodsInfo.objects.filter(gtype_id=3).order_by('-gclick')[:4]  # 热销商品
    Poultry_egg_products = GoodsInfo.objects.filter(gtype_id=4).order_by('-id')[:3]  # 最新商品
    Poultry_egg_products2 = GoodsInfo.objects.filter(gtype_id=4).order_by('-gclick')[:4]  # 热销商品
    Fresh_vegetables = GoodsInfo.objects.filter(gtype_id=5).order_by('-id')[:3]  # 最新商品
    Fresh_vegetables2 = GoodsInfo.objects.filter(gtype_id=5).order_by('-gclick')[:4]  # 热销商品
    Quick_frozenfood = GoodsInfo.objects.filter(gtype_id=6).order_by('-id')[:3]  # 最新商品
    Quick_frozenfood2 = GoodsInfo.objects.filter(gtype_id=6).order_by('-gclick')[:4]  # 热销商品

    context = {'title': '首页',

               'guest_cart': 1,
               'fruit': fruit,
               'fruit2': fruit2,
               'seafood':seafood,
               'seafood2': seafood2,
               'meat': meat,
               'meat2': meat2,
               'Poultry_egg_products': Poultry_egg_products,
               'Poultry_egg_products2': Poultry_egg_products2,
               'Fresh_vegetables': Fresh_vegetables,
               'Fresh_vegetables2': Fresh_vegetables2,
               'Quick_frozenfood': Quick_frozenfood,
               'Quick_frozenfood2': Quick_frozenfood2,
               'result': result,
               }
    return render(request, 'df_goods/index.html', context)

def goodslist(request,typeid,pageid,sort):
    goodType = TypeInfo.objects.get(id=typeid)
    uid = request.session.get('user_id')
    result = CartInfo.objects.filter(user_id=uid).count()
    sortdefault = 0
    sortprice = 0
    sortclick = 0
    # 获取最新发布的产品
    newgood = GoodsInfo.objects.filter(gtype_id=typeid).order_by('-id')[:2]
    if sort == '1':
        # 按id从大到小排序
        sumgoodList = GoodsInfo.objects.filter(gtype_id=typeid).order_by('-id')
        sortdefault = 1
    elif sort == '2':
        # 按价格从小到大排序
        sumgoodList = GoodsInfo.objects.filter(gtype_id=typeid).order_by('gprice')
        sortprice = 1
    elif sort == '3':
        # 按人气从大到小排序
        sumgoodList = GoodsInfo.objects.filter(gtype_id=typeid).order_by('-gclick')
        sortclick = 1

        # 分页 15个一页
    paginator = Paginator(sumgoodList, 4)
    pageid = int(pageid)  # 传入的参数pageid是字符串类型 需要转换成整型才能传递到page()方法中
    goodList = paginator.page(pageid)
    pindexlist = paginator.page_range  # 所有页的id
    context = {'title':'商品列表',

               'guest_cart': 1,
               'newgood': newgood,
               'goodList': goodList,
               'typeid': typeid,
               'sort': sort,
               'result': result,
               'sortdefault':sortdefault,
               'sortprice': sortprice,
               'sortclick': sortclick,
               'pindexlist': pindexlist,
               'pageid': pageid,
               'goodType': goodType,
               }
    return render(request, 'df_goods/list.html', context)

def detail(request, id):
    uid = request.session.get('user_id')
    result = CartInfo.objects.filter(user_id=uid).count()
    # 获取产品类型id
    typeid = GoodsInfo.objects.get(id=id).gtype_id
    # 获取产品类型
    goodType = TypeInfo.objects.get(id=typeid)
    # 获取最新发布的产品
    newgood = GoodsInfo.objects.filter(gtype_id=typeid).order_by('-id')[:2]
    # 获取当前商品的对象
    goods = GoodsInfo.objects.get(id=id)
    # 增加访问量（点击量gclick+1）
    goods.gclick += 1
    goods.save()
    context = {'title':'商品详情',
               'guest_cart':1,
                'result':result,
               'newgood': newgood,
               'typeid': typeid,
               'goodType': goodType,
               'goods':goods,
               'isDetail': 1
               }
    response = render(request, 'df_goods/detail.html', context)
    # 把当前商品的id存放到cookies中
    # 读取请求的cookie
    goods_ids = request.COOKIES.get('goods_ids')  # 5,2,7
    # 判断cookies中的商品id序列是否为空
    if goods_ids and goods_ids != '':
        # 不为空 以逗号分隔符把字符串转换为列表
        goods_ids = goods_ids.split(',')
        # 如果列表中已经有当前id 则需要删除列表中原来的id
        if id in goods_ids:
            goods_ids.remove(id)
        # 把当前的id插入到列表的最前面
        goods_ids.insert(0, id)
        # 取前5个
        if len(goods_ids) > 5:
            goods_ids = goods_ids[:5]
    else:
        # 为空
        goods_ids = id

    # 使用逗号来连接列表中的每个元素
    goods_ids = ','.join(goods_ids)
    # 添加cookies信息
    response.set_cookie('goods_ids', goods_ids)

    return response