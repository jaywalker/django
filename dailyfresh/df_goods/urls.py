from django.urls import path
from django.conf.urls import url
from . import views

# 子路由
urlpatterns = [
    url('^$', views.index, name='index'),
    url('^list(\d+)_(\d+)_(\d+)$', views.goodslist, name='list'),
    url('^(\d+)$', views.detail, name='detail'), # 商品详情页面
]