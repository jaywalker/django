# Generated by Django 3.2.16 on 2023-01-07 07:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('df_goods', '0001_initial'),
        ('df_user', '0002_auto_20230104_1605'),
    ]

    operations = [
        migrations.CreateModel(
            name='CartInfo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('count', models.IntegerField(default=0)),
                ('df_goods', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='df_goods.goodsinfo')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='df_user.userinfo')),
            ],
        ),
    ]
