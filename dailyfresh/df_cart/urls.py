from django.urls import path
from django.conf.urls import url
from . import views

# 子路由
urlpatterns = [
    url('^$', views.index, name='index'),
    url('^add(\d+)_(\d+)$', views.add, name='add'),
    url('^edit(\d+)_(\d+)$', views.edit, name='edit'),
    url('^delete(\d+)$', views.delete, name='delete'),

]