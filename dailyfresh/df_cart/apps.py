from django.apps import AppConfig


class DfCartConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'df_cart'
