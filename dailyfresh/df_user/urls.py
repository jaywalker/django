"""django1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views
from django.conf.urls import url
# 子路由
urlpatterns = [
    url('^register$', views.register, name='register'),
    url('^login$', views.login, name='login'),
    url('^register_handle$', views.register_handle, name='register_handle'),
    url('^register_exist$', views.register_exist, name='register_exist'),
    url('^login_handle$', views.login_handle, name='login_handle'),
    url('^info', views.info, name='info'),
    url('^order(\d+)$', views.order, name='order'),
    url('^site$', views.site, name='site'),
    url('^logout$', views.logout, name='logout'),
]
